# Amazon MemoryDB Connector

MemoryDB for Redis is a fully managed, Redis-compatible, in-memory database that delivers ultra-fast performance and Multi-AZ durability for modern applications built using microservices architectures.

Documentation: https://docs.aws.amazon.com/memory-db

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/memorydb/2021-01-01/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

